import java.util.Scanner;

public class task_9564 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        int[][] arr = new int[n][m];
        int[] total = new int[n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                arr[i][j] = scan.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            int sum = 0;
            for (int j = 0; j < m; j++) {
                sum += arr[i][j];
                total[i] = sum;
            }
        }
        int maxtotal = total[0];
        for (int i = 0; i < n; i++) {
            if (total[i]>maxtotal) {
                maxtotal = total[i];
            }
        }
        for (int i = 0; i < n; i++) {
            if (total[i]==maxtotal) {
                System.out.print(i+1+" ");
            }
        }
    }


}

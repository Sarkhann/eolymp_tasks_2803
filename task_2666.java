import java.util.Scanner;

public class task_2666 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[][] arr = new int[n][n];
        int m = 0, k = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i + j <= n - 2)
                    System.out.print("2");
                else if (i + j <= n - 1)
                    System.out.print("0");
                else if (i + j >= n)
                    System.out.print("1");
            }
            System.out.println("");
        }
    }
}

import java.util.Scanner;
public class task_9565 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        int [][] arr = new int[n][m];
        int [] arr1 = new int[n];
        for (int i=0; i<n; i++){
            for (int j=0; j<m; j++){
                arr[i][j] = scan.nextInt();
            }
        }
        for (int i=0; i<n; i++){
            int max = arr[i][0];
            for (int j=0; j<m; j++){
                if(arr[i][j] >= max){
                    max = arr[i][j];
                    arr1[i]=max;
                }
            }
            max =0;
        }
        int min = arr1[0];
        for (int i=0; i<n; i++){
            if (arr1[i]<=min)
                min = arr1[i];
        }
        System.out.println(min);

    }
}
